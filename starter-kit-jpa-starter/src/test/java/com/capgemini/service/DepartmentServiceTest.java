package com.capgemini.service;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dao.AddressDao;
import com.capgemini.domain.AddressEntity;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.DepartmentEntity;
import com.capgemini.domain.EmployeeEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class DepartmentServiceTest {

	@Autowired
	private DepartmentService mSUT;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private CarService carService;

	@Autowired
	private AddressDao addressDao;

	@Test
	public void shouldSaveDepartment() {
		// given
		int countBefore = mSUT.findAllDepartments().size();
		final AddressEntity address = addressDao.findOne(1L);
		final DepartmentEntity department = new DepartmentEntity("Added department", "123-123-123", "added@gmail.com",
				address);

		// when
		mSUT.saveDepartment(department);

		// then
		assertEquals(countBefore + 1, mSUT.findAllDepartments().size());
	}

	@Test
	public void shouldDeleteDepartment() {
		// given
		int countBefore = mSUT.findAllDepartments().size();
		DepartmentEntity department = mSUT.findDepartmentById(1L);

		// when
		mSUT.deleteDepartment(department);

		// then
		assertEquals(countBefore - 1, mSUT.findAllDepartments().size());
		assertNull(mSUT.findDepartmentById(department.getId()));
	}

	@Test
	public void shouldUpdateDepartment() {
		// given
		DepartmentEntity department = mSUT.findDepartmentById(1L);

		// when
		department.setDepartmentName("testName");
		mSUT.updateDepartment(department);

		// then
		DepartmentEntity updatedDepartment = mSUT.findDepartmentById(1L);
		assertEquals(updatedDepartment.getDepartmentName(), "testName");
	}

	@Test
	public void shouldFindEmployeesByDepartmentId() {
		// given when
		List<EmployeeEntity> employees = mSUT.findAllEmployeesByDepartmentId(3L);

		// then
		assertEquals(4, employees.size());
	}

	@Test
	public void shouldAddEmployeeToDepartment() {
		// given
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd");
		String date = "1950-01-01";
		LocalDate birthDate = LocalDate.parse(date, formatter);
		EmployeeEntity newEmployee = new EmployeeEntity("John", "Smith", birthDate, null, null, null, null);
		final AddressEntity address = addressDao.findOne(1L);
		DepartmentEntity department = new DepartmentEntity("newDepartment", "123-123-123", "newDepartment@gmail.com",
				address);

		// when
		mSUT.saveDepartment(department);
		employeeService.saveEmployee(newEmployee);
		mSUT.addEmployeeToDepartment(newEmployee, department);

		// then
		assertTrue(department.getEmployees().size() == 1);
		assertNotNull(newEmployee.getDepartment());
	}

	@Test
	public void shouldDeleteEmployeeFromDepartment() {
		// given
		DepartmentEntity department = mSUT.findDepartmentById(1L);
		EmployeeEntity employee = employeeService.findEmployeeById(21L);
		mSUT.addEmployeeToDepartment(employee, department);
		List<EmployeeEntity> employees = department.getEmployees();
		int countBefore = employees.size();

		// when
		mSUT.deleteEmployee(employee);

		// then
		assertEquals(countBefore - 1, department.getEmployees().size());
		assertNull(employee.getDepartment());
	}

	@Test
	public void shouldFindEmployeesInDepartmentByCar() {
		// given
		DepartmentEntity department = mSUT.findDepartmentById(1L);
		CarEntity car = carService.findCarById(1L);

		// when
		List<EmployeeEntity> employees = mSUT.findAllEmployeeByDepartmentAndCar(department, car);

		// then
		int expectedSize = 1;
		assertEquals(expectedSize, employees.size());
	}
}