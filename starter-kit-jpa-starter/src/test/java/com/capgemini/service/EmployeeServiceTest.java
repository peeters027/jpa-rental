package com.capgemini.service;

import static org.junit.Assert.*;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.domain.EmployeeEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class EmployeeServiceTest {

	@Autowired
	private EmployeeService mSUT;

	@Test
	public void shouldFindAllEmployeesUsingCriteria() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(null, null, null);
		assertEquals(21, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenAllCriteriaAreSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(1L, 1L, 1L);
		assertEquals(1, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenDepartmentAndCarAreSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(1L, null, 1L);
		assertEquals(1, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenDepartmentAndPositionAreSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(1L, 1L, null);
		assertEquals(4, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenCarAndPositionAreSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(null, 1L, 2L);
		assertEquals(1, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenDepartmentIsSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(1L, null, null);
		assertEquals(11, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenCarIsSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(null, null, 1L);
		assertEquals(1, employees.size());
	}

	@Test
	public void shouldFindEmployeesWhenPositionIsSpecified() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(null, 1L, null);
		assertEquals(9, employees.size());
	}

	@Test
	public void shouldFindAnyEmployeesFindingByNonExistCar() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(null, null, 6L);
		assertTrue(employees.isEmpty());
	}

	@Test
	public void shouldFindAnyEmployeesFindingByNonExistDepartment() {
		List<EmployeeEntity> employees = mSUT.findEmployeeByCriteria(6L, null, null);
		assertTrue(employees.isEmpty());
	}

}
