package com.capgemini.service;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dao.RentalDao;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.domain.RentalEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CarServiceTest {

	@PersistenceContext
	protected EntityManager entityManager;

	@Autowired
	private CarService mSUT;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private RentalDao rentalDao;

	@Test
	public void shouldSaveCar() {

		// given
		int countBefore = mSUT.findAllCars().size();
		final CarEntity car = new CarEntity("Opel", 1980, "red", 1900.0, 100, 200000, "Sedan");

		// when
		CarEntity savedCar = mSUT.saveCar(car);

		// then
		assertNotNull(savedCar.getId());
		assertEquals(countBefore + 1, mSUT.findAllCars().size());
	}

	@Test
	public void shouldDeleteCar() {

		// given
		int countBefore = mSUT.findAllCars().size();
		CarEntity car = mSUT.findCarById(1L);

		// when
		mSUT.deleteCar(car);
		List<RentalEntity> rents = rentalDao.findRenatlsByCarId(car.getId());
		int expedtedRentsSize = 0;

		// then
		assertEquals(countBefore - 1, mSUT.findAllCars().size());
		assertNull(mSUT.findCarById(car.getId()));
		assertEquals(expedtedRentsSize, rents.size());
	}

	@Test
	public void shouldUpdateCar() {

		// given
		CarEntity car = mSUT.findCarById(1L);

		// when
		car.setColor("gold");
		mSUT.updateCar(car);
		CarEntity updatedCar = mSUT.findCarById(1L);

		// then
		assertNotNull(mSUT.exists(1L));
		assertEquals(updatedCar.getColor(), "gold");
	}

	@Test
	public void testFindingByMarkAndChassis() {

		// given when
		List<CarEntity> cars = mSUT.findCarByMarkAndChassis("Fiat", "Limousine");

		// then
		Long expectedIndex = 17L;
		assertEquals(expectedIndex, cars.get(0).getId());
		assertEquals("blue", cars.get(0).getColor());
	}

	@Test
	public void testFindingByEmployee() {

		// given when
		List<CarEntity> cars = mSUT.findCarByEmployee(1L);

		// then
		assertEquals(1, cars.size());
	}

	@Test
	public void shouldAddEmployeeToCar() throws ParseException {

		// given
		CarEntity loadedCar = mSUT.findCarById(1L);
		EmployeeEntity loadedEmployee = employeeService.findEmployeeById(2L);

		// when then
		assertTrue(loadedCar.getEmployee().size() == 1);
		assertNotNull(loadedEmployee.getCar());
	}

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void testShouldThrowOptimisticLockingExceptionOnCarUpdate() {

		// given
		CarEntity firstCar = mSUT.findCarById(1L);
		CarEntity secondCar = mSUT.findCarById(1L);

		// when
		firstCar.setColor("silver");
		entityManager.detach(firstCar);

		secondCar.setColor("silver");
		entityManager.detach(secondCar);

		mSUT.updateCar(secondCar);
		entityManager.flush();
		mSUT.updateCar(firstCar);

		// then
		fail("OptimisticLockingException should have thrown!");
	}

	@Test
	public void shouldMakeCreationTime() {

		// given
		final CarEntity car = new CarEntity("Opel", 1980, "red", 1900.0, 100, 200000, "Sedan");

		// when
		CarEntity savedCar = mSUT.saveCar(car);
		entityManager.flush();

		// then
		assertNotNull(savedCar.getCreationDate());
	}

	@Test
	public void shouldMakeUpdateTime() {

		// given
		CarEntity car = mSUT.findCarById(1L);

		// when
		car.setColor("gold");
		mSUT.updateCar(car);
		entityManager.flush();

		// then
		assertEquals("gold", mSUT.findCarById(1L).getColor());
		assertNotNull(mSUT.findCarById(1L).getUpdateDate());
	}

	@Test
	public void shouldFindCarByNumberOfRents() {
		// given when
		Long numberOfRents = 1L;
		List<CarEntity> cars = mSUT.findCarByNumberOfRents(numberOfRents);

		// then
		assertEquals(2, cars.size());
	}

	@Test
	public void shouldFindCarByPeriod() {
		// given when
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd");
		String rentDate = "2015-05-01";
		String devotionDate = "2015-06-30";
		LocalDate rentTerm = LocalDate.parse(rentDate, formatter);
		LocalDate devotionTerm = LocalDate.parse(devotionDate, formatter);

		// then
		List<CarEntity> cars = mSUT.findCarRentedInSpecifiedPeriod(rentTerm, devotionTerm);
		assertEquals(1, cars.size());
	}
}
