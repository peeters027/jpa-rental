package com.capgemini;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.service.CarServiceTest;
import com.capgemini.service.DepartmentServiceTest;
import com.capgemini.service.EmployeeServiceTest;

@RunWith(Suite.class)
@SuiteClasses({ CarServiceTest.class, DepartmentServiceTest.class, EmployeeServiceTest.class })
public class AllTests {

}
