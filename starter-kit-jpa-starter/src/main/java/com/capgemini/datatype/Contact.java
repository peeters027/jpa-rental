package com.capgemini.datatype;

import javax.persistence.Embeddable;

@Embeddable
public class Contact {
	
	private String telephone;
	private String mail;
	
	public Contact(){	
	}
	
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
}
