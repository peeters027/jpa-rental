package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.DepartmentEntity;
import com.capgemini.domain.EmployeeEntity;

public interface DepartmentService {

	DepartmentEntity saveDepartment(DepartmentEntity department);

	void deleteDepartment(DepartmentEntity department);

	List<DepartmentEntity> findAllDepartments();

	DepartmentEntity findDepartmentById(Long id);

	DepartmentEntity updateDepartment(DepartmentEntity department);
	
	List<EmployeeEntity> findAllEmployeesByDepartmentId(Long id);
	
	DepartmentEntity addEmployeeToDepartment(EmployeeEntity employee, DepartmentEntity department);

	void deleteEmployee(EmployeeEntity employee);
	
	List<EmployeeEntity> findAllEmployeeByDepartmentAndCar(DepartmentEntity department, CarEntity car);
}
