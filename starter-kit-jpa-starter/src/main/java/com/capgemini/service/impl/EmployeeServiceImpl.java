package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.EmployeeService;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeRepository;

	@Override
	@Transactional(readOnly = false)
	public EmployeeEntity saveEmployee(EmployeeEntity employee) {
		employeeRepository.save(employee);
		return employee;
	}

	@Override
	public EmployeeEntity findEmployeeById(Long id) {
		return employeeRepository.findOne(id);
	}

	@Override
	public List<EmployeeEntity> findEmployeeByCriteria(Long idDep, Long idPos, Long idCar) {
		return employeeRepository.findEmployeeByCriteria(idDep, idPos, idCar);
	}

}
