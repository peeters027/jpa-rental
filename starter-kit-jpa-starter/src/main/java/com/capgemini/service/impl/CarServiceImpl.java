package com.capgemini.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.CarDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.CarService;

@Service
@Transactional(readOnly = true)
public class CarServiceImpl implements CarService {

	@Autowired
	private CarDao carRepository;

	@Autowired
	private EmployeeDao employeeRepository;

	@Override
	@Transactional(readOnly = false)
	public CarEntity saveCar(CarEntity car) {
		car = carRepository.save(car);
		return car;
	}

	@Override
	public List<CarEntity> findAllCars() {
		return carRepository.findAll();
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteCar(CarEntity car) {
		if (carRepository.exists(car.getId())) {
			carRepository.delete(car);
		}
	}

	@Override
	public CarEntity findCarById(Long id) {
		return carRepository.findOne(id);
	}

	@Override
	@Transactional(readOnly = false)
	public CarEntity updateCar(CarEntity car) {
		carRepository.update(car);
		return car;
	}

	@Override
	public boolean exists(Long id) {
		return carRepository.exists(id);
	}

	@Override
	public List<CarEntity> findCarByMarkAndChassis(String chassis, String mark) {
		List<CarEntity> cars = carRepository.findCarByMarkAndChassis(chassis, mark);
		return cars;
	}

	@Override
	public List<CarEntity> findCarByEmployee(Long id) {
		return carRepository.findCarByEmployee(id);
	}

	@Override
	@Transactional(readOnly = false)
	public CarEntity addEmployeeToCar(CarEntity car, EmployeeEntity employee) {

		car.addEmployeeToCar(car, employee);
		employee.addCar(employee, car);
		carRepository.update(car);
		employeeRepository.update(employee);

		return car;
	}

	@Override
	public List<CarEntity> findCarByNumberOfRents(Long numberOfRents) {
		return carRepository.findCarByNumberOfRents(numberOfRents);
	}

	@Override
	public List<CarEntity> findCarRentedInSpecifiedPeriod(LocalDate rentTerm, LocalDate devotionTerm) {
		return carRepository.findCarRentedInSpecifiedPeriod(rentTerm, devotionTerm);
	}
}
