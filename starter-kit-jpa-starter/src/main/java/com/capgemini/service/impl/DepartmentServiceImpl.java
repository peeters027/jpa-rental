package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.DepartmentDao;
import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.CarEntity;
import com.capgemini.domain.DepartmentEntity;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.service.DepartmentService;

@Service
@Transactional(readOnly = true)
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentRepository;

	@Autowired
	private EmployeeDao employeeRepository;

	@Override
	@Transactional(readOnly = false)
	public DepartmentEntity saveDepartment(DepartmentEntity department) {
		department = departmentRepository.save(department);
		return department;
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteDepartment(DepartmentEntity department) {
		if (departmentRepository.exists(department.getId())) {
			departmentRepository.delete(department);
		}
	}

	@Override
	public List<DepartmentEntity> findAllDepartments() {
		return departmentRepository.findAll();
	}

	@Override
	public DepartmentEntity findDepartmentById(Long id) {
		return departmentRepository.findOne(id);
	}

	@Override
	@Transactional(readOnly = false)
	public DepartmentEntity updateDepartment(DepartmentEntity department) {
		return departmentRepository.update(department);
	}

	@Override
	public List<EmployeeEntity> findAllEmployeesByDepartmentId(Long id) {
		return departmentRepository.findAllEmployeesByDepartment(id);

	}

	@Override
	@Transactional(readOnly = false)
	public DepartmentEntity addEmployeeToDepartment(EmployeeEntity employee, DepartmentEntity department) {
		department.addEmployeeToDepartment(employee, department);
		employee.addDepartment(employee, department);
		departmentRepository.update(department);
		employeeRepository.update(employee);
		return department;
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteEmployee(EmployeeEntity employee) {
		DepartmentEntity department = employee.getDepartment();
		department.deleteEmployee(employee);
		employee.setDepartment(null);
		departmentRepository.update(department);
		employeeRepository.update(employee);
	}

	@Override
	public List<EmployeeEntity> findAllEmployeeByDepartmentAndCar(DepartmentEntity department, CarEntity car) {
		return employeeRepository.findEmployeeByDepartmentAndCar(department.getId(), car.getId());
	}
}
