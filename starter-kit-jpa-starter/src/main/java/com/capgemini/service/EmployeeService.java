package com.capgemini.service;


import java.util.List;

import com.capgemini.domain.EmployeeEntity;

public interface EmployeeService {

	EmployeeEntity saveEmployee(EmployeeEntity employee);
	
	EmployeeEntity findEmployeeById(Long id);
	
	List<EmployeeEntity> findEmployeeByCriteria(Long idDep, Long idPos, Long idCar);
	
}
