package com.capgemini.service;

import java.time.LocalDate;
import java.util.List;

import com.capgemini.domain.CarEntity;
import com.capgemini.domain.EmployeeEntity;

public interface CarService {
	
	List<CarEntity> findAllCars(); 
	
	CarEntity saveCar(CarEntity car);
	
	void deleteCar(CarEntity car);
	
	CarEntity findCarById(Long id);
	
	CarEntity updateCar(CarEntity car);
	
	boolean exists(Long id);
	
	List<CarEntity> findCarByMarkAndChassis(String chassis, String mark);
	
	List<CarEntity> findCarByEmployee(Long id);
	
	CarEntity addEmployeeToCar(CarEntity car, EmployeeEntity employee);
	
	List<CarEntity> findCarByNumberOfRents(Long numberOfRents); 
	
	List<CarEntity> findCarRentedInSpecifiedPeriod(LocalDate rentTerm, LocalDate devotionTerm);
}
