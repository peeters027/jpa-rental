package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.RentalDao;
import com.capgemini.domain.RentalEntity;

@Repository
public class RentalDaoImpl extends AbstractDao<RentalEntity, Long> implements RentalDao {

	@Override
	public List<RentalEntity> findRenatlsByCarId(Long carId) {
		TypedQuery<RentalEntity> query = entityManager
				.createQuery("select r from RentalEntity r JOIN r.car rc WHERE rc.id= :idCar", RentalEntity.class);
		query.setParameter("idCar", carId);
		return query.getResultList();
	}
}
