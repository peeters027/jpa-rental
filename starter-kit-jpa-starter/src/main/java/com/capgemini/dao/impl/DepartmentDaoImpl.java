package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import com.capgemini.dao.DepartmentDao;
import com.capgemini.domain.DepartmentEntity;
import com.capgemini.domain.EmployeeEntity;

@Repository
public class DepartmentDaoImpl extends AbstractDao<DepartmentEntity, Long> implements DepartmentDao {

	@Override
	public List<EmployeeEntity> findAllEmployeesByDepartment(Long id) {
		TypedQuery<EmployeeEntity> query = entityManager.createQuery(
				"select e from EmployeeEntity e JOIN e.department d WHERE d.id= :id", EmployeeEntity.class);
        query.setParameter("id", id);
        return query.getResultList();
	}
}


