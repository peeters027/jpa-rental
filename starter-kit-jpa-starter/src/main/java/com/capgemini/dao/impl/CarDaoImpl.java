package com.capgemini.dao.impl;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import com.capgemini.dao.CarDao;
import com.capgemini.domain.CarEntity;

@Repository
public class CarDaoImpl extends AbstractDao<CarEntity, Long> implements CarDao {

	@Override
	public List<CarEntity> findCarByMarkAndChassis(String mark, String chassis) {
		TypedQuery<CarEntity> query = entityManager.createQuery(
				"Select car from CarEntity car where car.mark = :mark and car.chassis = :chassis", CarEntity.class);
		query.setParameter("mark", mark);
		query.setParameter("chassis", chassis);
		return query.getResultList();
	}

	@Override
	public List<CarEntity> findCarByEmployee(Long id) {
		TypedQuery<CarEntity> query = entityManager
				.createQuery("select car from CarEntity car JOIN car.employee ce WHERE ce.id= :id", CarEntity.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<CarEntity> findCarByNumberOfRents(Long numberOfRents) {

		TypedQuery<CarEntity> query = entityManager
				.createQuery("select car from CarEntity car JOIN car.rental cr GROUP BY car.id "
						+ "HAVING COUNT (DISTINCT cr.client) > :numberOfRents", CarEntity.class);
		query.setParameter("numberOfRents", numberOfRents);
		return query.getResultList();
	}

	@Override
	public List<CarEntity> findCarRentedInSpecifiedPeriod(LocalDate rentTerm, LocalDate devotionTerm) {
		TypedQuery<CarEntity> query = entityManager
				.createQuery("select car from CarEntity car JOIN car.rental cr where cr.rentTerm > :rentTerm "
						+ "and cr.devotionTerm < :devotionTerm", CarEntity.class);
		query.setParameter("rentTerm", rentTerm);
		query.setParameter("devotionTerm", devotionTerm);
		return query.getResultList();
	}
}
