package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.capgemini.dao.EmployeeDao;
import com.capgemini.domain.EmployeeEntity;
import com.capgemini.mapper.EmployeeRowMapper;

@Repository
public class EmployeeDaoImpl extends AbstractDao<EmployeeEntity, Long> implements EmployeeDao {

	private static final String FIND_EMPLOYEES_BY_DEPARTMENT_CAR_POSITION = "select e.id, e.first_name, e.surname from employee e JOIN employee_car AS ec where ec.car_id = :idCar AND ec.employee_id = e.id AND e.department_id = :idDep AND e.position_id = :idPos";
	private static final String FIND_EMPLOYEES_BY_DEPARTMENT_CAR = "select e.id, e.first_name, e.surname from employee e JOIN employee_car AS ec where ec.car_id = :idCar AND ec.employee_id = e.id AND e.department_id = :idDep";
	private static final String FIND_EMPLOYEES_BY_DEPARTMENT_POSITION = "select e.id, e.first_name, e.surname from employee e where e.department_id = :idDep AND e.position_id = :idPos";
	private static final String FIND_EMPLOYEES_BY_POSITION_CAR = "select e.id, e.first_name, e.surname from employee e JOIN employee_car AS ec where ec.car_id = :idCar AND ec.employee_id = e.id AND e.position_id = :idPos";
	private static final String FIND_EMPLOYEES_BY_CAR = "select e.id, e.first_name, e.surname from employee e JOIN employee_car AS ec where ec.car_id = :idCar AND ec.employee_id = e.id";
	private static final String FIND_EMPLOYEES_BY_DEPARTMENT = "select e.id, e.first_name, e.surname from employee e where e.department_id = :idDep";
	private static final String FIND_EMPLOYEES_BY_POSITION = "select e.id, e.first_name, e.surname from employee e where e.position_id = :idPos";
	private static final String FIND_ALL_EMPLOYEES = "select e.id, e.first_name, e.surname from employee e";

	@Autowired
	private NamedParameterJdbcOperations jdbcTemplate;

	@Autowired
	private EmployeeRowMapper employeeRowMapper;

	@Override
	public List<EmployeeEntity> findEmployeeByDepartmentAndCar(Long departmentId, Long carId) {
		TypedQuery<EmployeeEntity> query = entityManager.createQuery(
				"select e from EmployeeEntity e JOIN e.department ed JOIN e.car ec WHERE ed.id= :idDep and ec.id = :idCar",
				EmployeeEntity.class);
		query.setParameter("idDep", departmentId);
		query.setParameter("idCar", carId);
		return query.getResultList();
	}

	@Override
	public List<EmployeeEntity> findEmployeeByCriteria(Long idDep, Long idPos, Long idCar) {

		if (idDep != null && idPos != null && idCar != null) {
			SqlParameterSource params = new MapSqlParameterSource("idDep", idDep).addValue("idPos", idPos)
					.addValue("idCar", idCar);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_DEPARTMENT_CAR_POSITION, params, employeeRowMapper);
		}

		if (idDep != null && idPos == null && idCar != null) {
			SqlParameterSource params = new MapSqlParameterSource("idDep", idDep).addValue("idCar", idCar);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_DEPARTMENT_CAR, params, employeeRowMapper);
		}

		if (idDep != null && idPos != null && idCar == null) {
			SqlParameterSource params = new MapSqlParameterSource("idDep", idDep).addValue("idPos", idPos);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_DEPARTMENT_POSITION, params, employeeRowMapper);
		}

		if (idDep == null && idPos != null && idCar != null) {
			SqlParameterSource params = new MapSqlParameterSource("idCar", idCar).addValue("idPos", idPos);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_POSITION_CAR, params, employeeRowMapper);
		}

		if (idDep == null && idPos == null && idCar != null) {
			SqlParameterSource params = new MapSqlParameterSource("idCar", idCar);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_CAR, params, employeeRowMapper);
		}

		if (idDep != null && idPos == null && idCar == null) {
			SqlParameterSource params = new MapSqlParameterSource("idDep", idDep);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_DEPARTMENT, params, employeeRowMapper);
		}

		if (idDep == null && idPos != null && idCar == null) {
			SqlParameterSource params = new MapSqlParameterSource("idPos", idPos);
			return jdbcTemplate.query(FIND_EMPLOYEES_BY_POSITION, params, employeeRowMapper);
		}

		if (idDep == null && idPos == null && idCar == null) {
			return jdbcTemplate.query(FIND_ALL_EMPLOYEES, employeeRowMapper);
		}
		return null;
	}

}