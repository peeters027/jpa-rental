package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.AddressDao;
import com.capgemini.domain.AddressEntity;

@Repository
public class AddressDaoImpl extends AbstractDao<AddressEntity, Long> implements AddressDao {

}
