package com.capgemini.dao;

import java.time.LocalDate;
import java.util.List;

import com.capgemini.domain.CarEntity;

public interface CarDao extends Dao<CarEntity, Long> {

	List<CarEntity> findCarByMarkAndChassis(String mark, String chassis);

	List<CarEntity> findCarByEmployee(Long id);

	List<CarEntity> findCarByNumberOfRents(Long numberOfRents);

	List<CarEntity> findCarRentedInSpecifiedPeriod(LocalDate rentTerm, LocalDate devotionTerm);
}
