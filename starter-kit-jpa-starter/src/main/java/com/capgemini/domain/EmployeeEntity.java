package com.capgemini.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.capgemini.datatype.Contact;

@Entity
@Table(name = "employee")
public class EmployeeEntity extends AbstractEntity {

	@Column(length = 30, nullable = false)
	private String firstName;

	@Column(length = 30, nullable = false)
	private String surname;

	@Column(nullable = false)
	private LocalDate birthDate;

	@ManyToOne
	private DepartmentEntity department;

	@OneToOne
	private PositionEntity position;

	@ManyToMany
	private List<CarEntity> car;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "mail", column = @Column(name = "mail", length = 45) ),
			@AttributeOverride(name = "telephone", column = @Column(name = "telephone", length = 11) ) })
	private Contact contact;

	public EmployeeEntity() {
	}

	public EmployeeEntity(String firstName, String surname, LocalDate birthDate, DepartmentEntity department,
			PositionEntity position, List<CarEntity> car, Contact contact) {
		super();
		this.firstName = firstName;
		this.surname = surname;
		this.birthDate = birthDate;
		this.department = department;
		this.position = position;
		this.car = car;
		this.contact = contact;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public DepartmentEntity getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}

	public PositionEntity getPosition() {
		return position;
	}

	public void setPosition(PositionEntity position) {
		this.position = position;
	}

	public List<CarEntity> getCar() {
		return car;
	}

	public void setCar(List<CarEntity> car) {
		this.car = car;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public EmployeeEntity addDepartment(EmployeeEntity employee, DepartmentEntity department) {
		employee.setDepartment(department);
		return employee;
	}

	public EmployeeEntity addCar(EmployeeEntity employee, CarEntity car) {
		this.car.add(car);
		return employee;
	}
}
