package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class AddressEntity extends AbstractEntity {

	@Column(length = 45, nullable = false)
	private String street;

	@Column(nullable = false)
	private Integer buildingNumber;

	@Column
	private Integer flatNumber;

	@Column(length = 30, nullable = false)
	private String postCode;

	@Column(length = 30, nullable = false)
	private String city;

	protected AddressEntity() {
	}

	public AddressEntity(String street, Integer buildingNumber, Integer flatNumber, String postCode, String city) {
		super();
		this.street = street;
		this.buildingNumber = buildingNumber;
		this.flatNumber = flatNumber;
		this.postCode = postCode;
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(Integer buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public Integer getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(Integer flatNumber) {
		this.flatNumber = flatNumber;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
