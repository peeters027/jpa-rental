package com.capgemini.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class DepartmentEntity extends AbstractEntity {

	@Column(length = 30, nullable = false)
	private String departmentName;

	@Column(length = 11, nullable = false)
	private String telephone;

	@Column(length = 30, nullable = false)
	private String mail;

	@OneToOne
	private AddressEntity address;

	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	private List<EmployeeEntity> employees;

	@OneToMany(mappedBy = "departmentFrom", cascade = CascadeType.ALL)
	private List<RentalEntity> rentalFrom;

	@OneToMany(mappedBy = "departmentTo", cascade = CascadeType.ALL)
	private List<RentalEntity> renatlTo;

	public DepartmentEntity() {
	}

	public DepartmentEntity(String departmentName, String telephone, String mail, AddressEntity address) {
		super();
		this.departmentName = departmentName;
		this.telephone = telephone;
		this.mail = mail;
		this.address = address;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public List<EmployeeEntity> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeEntity> employees) {
		this.employees = employees;
	}

	public void addEmployee(EmployeeEntity employee) {
		if (this.employees == null) {
			employees = new ArrayList<>();
		}

		this.employees.add(employee);
	}

	public DepartmentEntity addEmployeeToDepartment(EmployeeEntity employee, DepartmentEntity department) {

		department.addEmployee(employee);
		return department;
	}

	public void deleteEmployee(EmployeeEntity employee) {
		if (this.employees != null) {
			employees.remove(employee);
		}
	}

}
