package com.capgemini.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rent")
public class RentalEntity extends AbstractEntity {

	@Column(length = 10)
	private Integer expenseInDollars;

	@Column(length = 10, nullable = false)
	private LocalDate rentTerm;

	@Column
	private LocalDate devotionTerm;

	@ManyToOne
	@JoinColumn(name = "department_from_id")
	private DepartmentEntity departmentFrom;

	@ManyToOne
	@JoinColumn(name = "department_to_id")
	private DepartmentEntity departmentTo;

	@ManyToOne
	private CarEntity car;

	@ManyToOne
	private ClientEntity client;

	public Integer getExpenseInDollars() {
		return expenseInDollars;
	}

	public void setExpenseInDollars(Integer expenseInDollars) {
		this.expenseInDollars = expenseInDollars;
	}

	public LocalDate getRentTerm() {
		return rentTerm;
	}

	public void setRentTerm(LocalDate rentTerm) {
		this.rentTerm = rentTerm;
	}

	public LocalDate getDevotionTerm() {
		return devotionTerm;
	}

	public void setDevotionTerm(LocalDate devotionTerm) {
		this.devotionTerm = devotionTerm;
	}

	public DepartmentEntity getDepartmentFrom() {
		return departmentFrom;
	}

	public void setDepartmentFrom(DepartmentEntity departmentFrom) {
		this.departmentFrom = departmentFrom;
	}

	public DepartmentEntity getDepartmentTo() {
		return departmentTo;
	}

	public void setDepartmentTo(DepartmentEntity departmentTo) {
		this.departmentTo = departmentTo;
	}

	public CarEntity getCar() {
		return car;
	}

	public void setCar(CarEntity car) {
		this.car = car;
	}

	public ClientEntity getClient() {
		return client;
	}

	public void setClient(ClientEntity client) {
		this.client = client;
	}

}
