package com.capgemini.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "car")
public class CarEntity extends AbstractEntity {

	@Column(length = 30, nullable = false)
	private String mark;

	@Column(length = 11, nullable = false)
	private Integer yearProduce;

	@Column(length = 30, nullable = false)
	private String color;

	@Column(precision = 2, scale = 1, nullable = false)
	private Double engineCapacity;

	@Column(length = 11, nullable = false)
	private Integer powerInKw;

	@Column(length = 11, nullable = false)
	private Integer milage;

	@Column(length = 30, nullable = false)
	private String chassis;

	@ManyToMany(mappedBy = "car", cascade = CascadeType.ALL)
	private List<EmployeeEntity> employee;
	
	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
	private List<RentalEntity> rental;

	protected CarEntity() {
	}

	public List<RentalEntity> getRental() {
		return rental;
	}

	public void setRental(List<RentalEntity> rental) {
		this.rental = rental;
	}

	public CarEntity(String mark, Integer yearProduce, String color, Double engineCapacity, Integer powerInKw,
			Integer milage, String chassis) {
		super();
		this.mark = mark;
		this.yearProduce = yearProduce;
		this.color = color;
		this.engineCapacity = engineCapacity;
		this.powerInKw = powerInKw;
		this.milage = milage;
		this.chassis = chassis;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public Integer getYearProduce() {
		return yearProduce;
	}

	public void setYearProduce(Integer yearProduce) {
		this.yearProduce = yearProduce;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Double getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(Double engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public Integer getPowerInKw() {
		return powerInKw;
	}

	public void setPowerInKw(Integer powerInKw) {
		this.powerInKw = powerInKw;
	}

	public Integer getMilage() {
		return milage;
	}

	public void setMilage(Integer milage) {
		this.milage = milage;
	}

	public List<EmployeeEntity> getEmployee() {
		return employee;
	}

	public void setEmployee(List<EmployeeEntity> employee) {
		this.employee = employee;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public void addEmployee(EmployeeEntity employee) {
		if (this.employee == null) {
			this.employee = new ArrayList<>();
		}

		this.employee.add(employee);
	}

	public CarEntity addEmployeeToCar(CarEntity car, EmployeeEntity employee) {
		car.addEmployee(employee);
		return car;
	}
}
