package com.capgemini.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "job_title")
public class PositionEntity extends AbstractEntity {

	@Column(length = 30, nullable = false)
	private String namePosition;

	public String getNamePosition() {
		return namePosition;
	}

	public void setNamePosition(String namePosition) {
		this.namePosition = namePosition;
	}

	protected PositionEntity() {
	}

	public PositionEntity(String namePosition) {
		super();
		this.namePosition = namePosition;
	}

}
