package com.capgemini.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractEntity {

	@Version
	@Column(name = "version", nullable = false, columnDefinition = "INTEGER DEFAULT 0")
	private int version;

	@Column(name = "creationDate", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date creationDate;

	@Column(name = "updateDate", columnDefinition = "DATETIME DEFAULT NULL")
	private Date updateDate;

	@Id
	@GeneratedValue
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@PreUpdate
	private void setUpdateDate() {
		Date date = new Date();
		updateDate = date;
	}

	@PrePersist
	private void setCreationDate() {
		Date date = new Date();
		creationDate = date;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
}
