package com.capgemini.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.capgemini.datatype.Contact;

@Entity
@Table(name = "clients")
public class ClientEntity extends AbstractEntity {

	@Column(length = 30, nullable = false)
	private String firstName;

	@Column(length = 30, nullable = false)
	private String surname;

	@Column(length = 19, nullable = false)
	private String creditCard;

	@Column(nullable = false)
	private LocalDate birthDate;

	@OneToOne
	private AddressEntity address;

	@OneToMany(mappedBy = "client")
	private List<RentalEntity> rental;

	protected ClientEntity() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "mail", column = @Column(name = "mail", length = 30, nullable = false) ),
			@AttributeOverride(name = "telephone", column = @Column(name = "telephone", length = 11, nullable = false) ) })
	private Contact contact;
}
