package com.capgemini.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.capgemini.domain.EmployeeEntity;

@Component
public class EmployeeRowMapper implements RowMapper<EmployeeEntity> {

	@Override
	public EmployeeEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmployeeEntity employee = new EmployeeEntity();
		employee.setId(rs.getLong(1));
		employee.setFirstName(rs.getString(2));
		employee.setSurname(rs.getString(3));
		return employee;
	}

}
